package com.example.movie.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USERS_ROLES")
public class UserRole {
    @Id
    @Column(name = "USER_ID")
    private long userId;
    @Id
    @Column(name = "ROLE_ID")
    private long roleId;
}
