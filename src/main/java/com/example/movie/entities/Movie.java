package com.example.movie.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MOVIES")
public class Movie {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "IMAGE_URL")
    private String imageUrl;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "DESC")
    private String desc;
    @Column(name = "NUMBER_VIEW")
    private Long numberView;
    @Column(name = "TYPE_ID")
    private Long typeId;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CREATE_BY")
    private String createBy;
    @Column(name = "CREATE_AT")
    private Date createAt;
    @Column(name = "UPDATE_BY")
    private String updateBy;
    @Column(name = "UPDATE_AT")
    private Date updateAt;
}
