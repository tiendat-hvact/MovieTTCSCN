package com.example.movie.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "REPORTS")
public class Report {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "MOVIE_ID")
    private Long movieId;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CREATE_AT")
    private Date createAt;
    @Column(name = "UPDATE_BY")
    private Date updateBy;
    @Column(name = "UPDATE_AT")
    private Date updateAt;
}
