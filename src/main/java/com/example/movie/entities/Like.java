package com.example.movie.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LIKES")
public class Like {
    @Id
    @Column(name = "USER_ID")
    private Long userId;
    @Id
    @Column(name = "MOVIE_ID")
    private Long movieId;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CREATE_AT")
    private Date createAt;
    @Column(name = "UPDATE_AT")
    private Date updateAt;
}
