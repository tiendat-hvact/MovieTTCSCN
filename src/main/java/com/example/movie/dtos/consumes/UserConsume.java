package com.example.movie.dtos.consumes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserConsume {
    private Long id;
    private String account;
    private String password;
    private String email;
    private String history;
}
