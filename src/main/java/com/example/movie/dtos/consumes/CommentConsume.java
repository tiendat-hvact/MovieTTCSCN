package com.example.movie.dtos.consumes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentConsume {
    private Long id;
    private String content;
    private long parentId;
    private long userId;
    private long movieId;
}
