package com.example.movie.dtos.consumes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateConsume {
    private Long userId;
    private Long movieId;
    private Long rate;
    private String content;
}
