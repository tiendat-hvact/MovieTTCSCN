package com.example.movie.dtos.consumes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieConsume {
    private Long id;
    private String imageUrl;
    private String title;
    private String desc;
    private Long numberView;
    private Long typeId;
    private Long categoryId;
}
