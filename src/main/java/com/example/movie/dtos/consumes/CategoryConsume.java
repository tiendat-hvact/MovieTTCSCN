package com.example.movie.dtos.consumes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryConsume {
    private Long id;
    private String name;
}
