package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikeProduce {
    private Long userId;
    private Long movieId;
    private String status;
    private Date createAt;
    private Date updateAt;
}
