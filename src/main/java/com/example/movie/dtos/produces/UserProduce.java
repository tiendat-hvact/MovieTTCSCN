package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProduce {
    private Long id;
    private String account;
    private String password;
    private String email;
    private String history;
    private String status;
    private String createBy;
    private Date createAt;
    private String updateBy;
    private Date updateAt;
}
