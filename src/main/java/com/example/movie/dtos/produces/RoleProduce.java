package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleProduce {
    private long userId;
    private long roleId;
}
