package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieProduce {
    private Long id;
    private String imageUrl;
    private String title;
    private String desc;
    private Long numberView;
    private Long typeId;
    private Long categoryId;
    private String status;
    private String createBy;
    private Date createAt;
    private String updateBy;
    private Date updateAt;
}
