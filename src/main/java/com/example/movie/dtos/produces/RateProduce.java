package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateProduce {
    private Long userId;
    private Long movieId;
    private Long rate;
    private String content;
    private String status;
    private Date createAt;
    private Date updateAt;
}
