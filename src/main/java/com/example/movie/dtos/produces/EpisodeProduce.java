package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeProduce {
    private Long id;
    private String name;
    private String url;
    private Long movieId;
    private String status;
    private String createBy;
    private Date createAt;
    private String updateBy;
    private Date updateAt;
}
