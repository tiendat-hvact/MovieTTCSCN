package com.example.movie.dtos.produces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentProduce {
    private Long id;
    private String content;
    private long parentId;
    private long userId;
    private long movieId;
    private String status;
    private String createBy;
    private Date createAt;
    private String updateBy;
    private Date updateAt;
}
