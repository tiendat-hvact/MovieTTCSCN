package com.example.movie.common.utils;

import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {
    public static final String CUS_SEGM = "CUS_SEGM";
    public static final String FILE_STORAGE = "FILE_STORAGE";
    public static final String FILE_STORAGE_PATH = "/Users/nguyenlinh/Project/downloadFile/template/";
    public static final String FILE_STORAGE_LOCAL = "FILE_STORAGE_LOCAL";
    public static final String FILE_TEMPLATE = "FILE_TEMPLATE";
    public static final String LOAN_SUGGESST_MAIL_ERROR_SUPPLIER = "LOAN_SUGGESST_MAIL_ERROR_SUPPLIER";
    public static final String LOAN_SUGGESST_MAIL_ERROR_ANCHOR = "LOAN_SUGGESST_MAIL_ERROR_ANCHOR";
    public static final String NF_SOURCE_RATE = "NF_SOURCE_RATE";
    public static final String FUND_SOURCE = "FUND_SOURCE";
    public static final String FUND_SOURCE_USE = "FUND_SOURCE_USE";
    public static final String LDAP_TYPE = "AUTHEN_LDAP";
    public static final String FORMAT_DATE_yyyyMMdd = "yyyyMMdd";
    public static final String FORMAT_DATE_ddMMyyyy = "dd/MM/yyyy";
    public static final String FORMAT_DATE_ddMMMyyyy = "dd/MMM/yyyy";
    public static final String FORMAT_DATE_ddMMyyyyHHmmss = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_DATE_yyyyMMddTHHmmss = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT_DATE_yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_yyyyMMDDHHmmss = "yyyyMMddHHmmss";
    public static final String FORMAT_DATE_yyyyMMDDHHmmssSSS = "yyyyMMddHHmmssSSS";
    private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private static final int RANDOM_STRING_LENGTH = 10;

    private static final char DEFAULT_TRIM_WHITESPACE = ' ';

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
    private static final int PAD_LIMIT = 8192;
    private static SimpleDateFormat sdf;

    public static String toSlug(String input, Date inDate) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("_");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH) + "_" + convertDate2String(inDate);
    }

    public static String convertDate2String(Date inDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        try {
            return formatter.format(inDate);

        } catch (Exception e) {
            return "";
        }
    }

    public static String convertDate2String(Date inDate, String sformat) {
        SimpleDateFormat formatter = new SimpleDateFormat(sformat);
        if (inDate == null)
            return "";
        try {
            return formatter.format(inDate);

        } catch (Exception e) {
            return "";
        }
    }

    /**
     * This method generates random string
     *
     * @return
     */

    public static Date getDayBefore(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    /**
     * Trims the character given from both left and right of the string given. For
     * trimming whitespace you can simply use the String classes trim method.
     *
     * @param string   The string to trim characters from, cannot be null.
     * @param trimChar The character to trim from either side of the given string.
     * @return A string with the given characters trimmed from either side.
     */
    public static String trim(final String string, final char trimChar) {
        return trimLeft(trimRight(string, trimChar), trimChar);
    }

    /**
     * Trims spaces from the left side of the string and returns the result.
     *
     * @param string The string to trim.
     * @return A string with all spaces removed from the left side.
     */
    public static String trimLeft(String string) {
        return trimLeft(string, DEFAULT_TRIM_WHITESPACE);
    }

    /**
     * Trims the character given from the given string and returns the result.
     *
     * @param string   The string to trim, cannot be null.
     * @param trimChar The character to trim from the left of the given string.
     * @return A string with the given character trimmed from the string given.
     */
    public static String trimLeft(final String string, final char trimChar) {
        final int stringLength = string.length();
        int i;

        for (i = 0; i < stringLength && string.charAt(i) == trimChar; i++) {
            /*
             * increment i until it is at the location of the first char that does not match
             * the trimChar given.
             */
        }

        if (i == 0) {
            return string;
        } else {
            return string.substring(i);
        }
    }

    /**
     * Trims spaces from the right side of the string given and returns the result.
     *
     * @param string The string to trim, cannot be null.
     * @return A string with all whitespace trimmed from the right side of it.
     */
    public static String trimRight(final String string) {
        return trimRight(string, DEFAULT_TRIM_WHITESPACE);
    }

    /**
     * Trims the character given from the right side of the string given. The result
     * of this trimming is then returned.
     *
     * @param string   The string to trim, cannot be null.
     * @param trimChar The character to trim from the right side of the given string.
     * @return The result of trimming the character given from the right side of the
     * given string.
     */
    public static String trimRight(final String string, final char trimChar) {
        final int lastChar = string.length() - 1;
        int i;

        for (i = lastChar; i >= 0 && string.charAt(i) == trimChar; i--) {
            /*
             * Decrement i until it is equal to the first char that does not match the
             * trimChar given.
             */
        }

        if (i < lastChar) {
            // the +1 is so we include the char at i
            return string.substring(0, i + 1);
        } else {
            return string;
        }
    }

    public static String getSDate(String sFormat) {
        sdf = new SimpleDateFormat(sFormat);
        Date dLocalDate = new Date();
        return sdf.format(dLocalDate);
    }

    public static String getSDateYYYYMMDDHHmmss() {
        Date dLocalDate = new Date();
        sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(dLocalDate);
    }

    public static String getSDateYYYYMMDDHHmmssSSS() {
        Date dLocalDate = new Date();
        sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(dLocalDate);
    }

    public static String getSDateYYYYMMDDHHmmssSSSS() {
        Date dLocalDate = new Date();
        sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
        return sdf.format(dLocalDate);
    }

    public static String convertDate1(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateDraftFormat = new SimpleDateFormat("ddMMyy");
        Date temp = dateFormat.parse(date);
        return dateDraftFormat.format(temp);
    }

    public static String convertDate(String date, String fromFormat, String toFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(fromFormat);
        SimpleDateFormat dateDraftFormat = new SimpleDateFormat(toFormat);
        Date temp;
        try {
            temp = dateFormat.parse(date);
            return dateDraftFormat.format(temp);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return date;
        }

    }

    public static String convertPath(String path) {
        if (File.separator.equals("/")) {
            path = path.replaceAll("\\\\", "/");
        } else {
            path = path.replaceAll("/", File.separator + File.separator);
        }
        return path;
    }

    public static Date parseDate(String time) throws ParseException {
        return (new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss")).parse(time);
    }

    public static Date parseDateddMMyy(String time) throws ParseException {
        return (new SimpleDateFormat("ddMMyy")).parse(time);
    }

    /**
     * @param date
     * @return "dd/MM/yyyy"
     */

    public static String dateToString(Date date) {
        if (date == null) return "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    public static String timeToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH-mm-ss");
        return dateFormat.format(date);
    }

    public static Date getDate(int deflection) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, deflection);
        return cal.getTime();
    }

    public static Date getDateCurrent() {
        return Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Left pad a String with a specified String.
     * <p>
     * Pad to a size of <code>size</code>.
     *
     * <pre>
     * StringUtils.leftPad(null, *, *)      = null
     * StringUtils.leftPad("", 3, "z")      = "zzz"
     * StringUtils.leftPad("bat", 3, "yz")  = "bat"
     * StringUtils.leftPad("bat", 5, "yz")  = "yzbat"
     * StringUtils.leftPad("bat", 8, "yz")  = "yzyzybat"
     * StringUtils.leftPad("bat", 1, "yz")  = "bat"
     * StringUtils.leftPad("bat", -1, "yz") = "bat"
     * StringUtils.leftPad("bat", 5, null)  = "  bat"
     * StringUtils.leftPad("bat", 5, "")    = "  bat"
     * </pre>
     *
     * @param str    the String to pad out, may be null
     * @param size   the size to pad to
     * @param padStr the String to pad with, null or empty treated as single space
     * @return left padded String or original String if no padding is necessary,
     * <code>null</code> if null String input
     */
    public static String leftPad(String str, int size, String padStr) {
        if (str == null) {
            return null;
        }
        if (isEmpty(padStr)) {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return leftPad(str, size, padStr.charAt(0));
        }

        if (pads == padLen) {
            return padStr.concat(str);
        } else if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        } else {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return new String(padding).concat(str);
        }
    }

    public static String leftPadNotNull(String str, int size, String padStr) {
        if (str == null) {
            str = "";
        }
        if (isEmpty(padStr)) {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return leftPad(str, size, padStr.charAt(0));
        }

        if (pads == padLen) {
            return padStr.concat(str);
        } else if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        } else {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return new String(padding).concat(str);
        }
    }

    /**
     * Left pad a String with a specified character.
     * <p>
     * Pad to a size of <code>size</code>.
     *
     * <pre>
     * StringUtils.leftPad(null, *, *)     = null
     * StringUtils.leftPad("", 3, 'z')     = "zzz"
     * StringUtils.leftPad("bat", 3, 'z')  = "bat"
     * StringUtils.leftPad("bat", 5, 'z')  = "zzbat"
     * StringUtils.leftPad("bat", 1, 'z')  = "bat"
     * StringUtils.leftPad("bat", -1, 'z') = "bat"
     * </pre>
     *
     * @param str     the String to pad out, may be null
     * @param size    the size to pad to
     * @param padChar the character to pad with
     * @return left padded String or original String if no padding is necessary,
     * <code>null</code> if null String input
     * @since 2.0
     */

    public static String leftPad(String str, int size, char padChar) {
        if (str == null) {
            return null;
        }
        int pads = size - str.length();
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (pads > PAD_LIMIT) {
            return leftPad(str, size, String.valueOf(padChar));
        }
        return padding(pads, padChar).concat(str);
    }

    private static String padding(int repeat, char padChar) throws IndexOutOfBoundsException {
        if (repeat < 0) {
            throw new IndexOutOfBoundsException("Cannot pad a negative amount: " + repeat);
        }
        final char[] buf = new char[repeat];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = padChar;
        }
        return new String(buf);
    }

    /**
     * Right pad a String with a specified character.
     * <p>
     * The String is padded to the size of <code>size</code>.
     *
     * <pre>
     * StringUtils.rightPad(null, *, *)     = null
     * StringUtils.rightPad("", 3, 'z')     = "zzz"
     * StringUtils.rightPad("bat", 3, 'z')  = "bat"
     * StringUtils.rightPad("bat", 5, 'z')  = "batzz"
     * StringUtils.rightPad("bat", 1, 'z')  = "bat"
     * StringUtils.rightPad("bat", -1, 'z') = "bat"
     * </pre>
     *
     * @param str     the String to pad out, may be null
     * @param size    the size to pad to
     * @param padChar the character to pad with
     * @return right padded String or original String if no padding is necessary,
     * <code>null</code> if null String input
     * @since 2.0
     */
    public static String rightPad(String str, int size, char padChar) {
        if (str == null) {
            return null;
        }
        int pads = size - str.length();
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (pads > PAD_LIMIT) {
            return rightPad(str, size, String.valueOf(padChar));
        }
        return str.concat(padding(pads, padChar));
    }

    /**
     * Right pad a String with a specified String.
     * <p>
     * The String is padded to the size of <code>size</code>.
     *
     * <pre>
     * StringUtils.rightPad(null, *, *)      = null
     * StringUtils.rightPad("", 3, "z")      = "zzz"
     * StringUtils.rightPad("bat", 3, "yz")  = "bat"
     * StringUtils.rightPad("bat", 5, "yz")  = "batyz"
     * StringUtils.rightPad("bat", 8, "yz")  = "batyzyzy"
     * StringUtils.rightPad("bat", 1, "yz")  = "bat"
     * StringUtils.rightPad("bat", -1, "yz") = "bat"
     * StringUtils.rightPad("bat", 5, null)  = "bat  "
     * StringUtils.rightPad("bat", 5, "")    = "bat  "
     * </pre>
     *
     * @param str    the String to pad out, may be null
     * @param size   the size to pad to
     * @param padStr the String to pad with, null or empty treated as single space
     * @return right padded String or original String if no padding is necessary,
     * <code>null</code> if null String input
     */
    public static String rightPad(String str, int size, String padStr) {
        if (str == null) {
            return null;
        }
        if (isEmpty(padStr)) {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return rightPad(str, size, padStr.charAt(0));
        }

        if (pads == padLen) {
            return str.concat(padStr);
        } else if (pads < padLen) {
            return str.concat(padStr.substring(0, pads));
        } else {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return str.concat(new String(padding));
        }
    }

    // Empty checks
    // -----------------------------------------------------------------------

    /**
     * Checks if a String is empty ("") or null.
     *
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty("")        = true
     * StringUtils.isEmpty(" ")       = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ") = false
     * </pre>
     * <p>
     * NOTE: This method changed in Lang version 2.0. It no longer trims the String.
     * That functionality is available in isBlank().
     *
     * @param str the String to check, may be null
     * @return <code>true</code> if the String is empty or null
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static Object preferValue(Object prefer1, Object prefer2) {
        if (prefer1 == null || (prefer1 instanceof String && ((String) prefer1).length() == 0)) {
            return prefer2;
        }
        return prefer1;
    }

    public static String leftPad(String s, int width) {
        return String.format("%" + width + "s", s).replace(' ', ' ');
    }

    public static String rightPad(String pad, String s, int width) {
        return String.format("%-" + width + "s", s).replace(" ", pad);
    }

    public static String LPad(String str, Long length, char car) {
        return String.format("%" + (length - str.length()) + "s", "").replace(" ", String.valueOf(car)) + str;
    }

    public static Date convertStringToDate(String dateStr, String format) {
        SimpleDateFormat mySimpleDateFormat = new SimpleDateFormat(format);
        try {
            return mySimpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date formatDate(Date dateConvert, String pattern) {
        try {
            SimpleDateFormat fm = new SimpleDateFormat(pattern);
            String strDate = fm.format(dateConvert);
            return fm.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDateToString(Date date, String format) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String formatDate(Date date, SimpleDateFormat dateFormat) {
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isNullAllPropertiesInObject(Object obj) {
        try {
            for (Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (f.get(obj) != null) {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getLastChar(String string) {
        if (string == null) return "";
        if (string.lastIndexOf(" ") < 0) return "";
        return string.substring(string.lastIndexOf(" ") + 1);
    }


    public static String getFirstChar(String string) {
        if (string == null) return "";
        if (string.lastIndexOf(" ") < 0) return string;
        return string.substring(0, string.lastIndexOf(" "));
    }

    public static String getCardNumToString(String string, int number) {
        String temp = "";
        int lenthString = string.length();
        int count = lenthString / number;
        for (int i = 0; i < count; i++) {
            temp += string.substring(i * number, ((i + 1) * number)) + " ";
        }

        return temp;
    }

    // NgocTV add
    public static String convertDateToStringRC(String date) {

        String temp = date.substring(0, 2) + date.substring(3, 5) + date.substring(8, 10);
        return temp;
    }

    // dungnd4
    // Convert Date to Calendar
    public static Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }

    // Convert Calendar to Date
    public static Date calendarToDate(Calendar calendar) {
        return calendar.getTime();
    }

    public static boolean isStringContains(String str, String[] fillterArray) {
        boolean isMatche = true;
        for (String keyWord : fillterArray) {
            if (!str.contains(keyWord)) {
                isMatche = false;
                break;
            }
        }
        return isMatche;
    }

    public static String getSDate(Date dLocalDate, String sFormat) {
        sdf = new SimpleDateFormat(sFormat);

        return sdf.format(dLocalDate);
    }

    // dungnd loai bo khoang trang
    public static String trimSpace(String str) {
        return str.replaceAll("\\s+", "");
    }

    public static void main(String[] args) {
        int n;


    }

    public static String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z 0-9]");
        Matcher matcher = pattern.matcher(s);
        String str = matcher.replaceAll("").replaceAll("\\s+", " ");
        return str.trim();
    }

    // dungnd4 show cardNumber
    public static String replaceAllCardNumber(String s) {
        String a = s.substring(6, 12);
        String b = s.substring(0, 6);
        String c = s.substring(12, 16);
        String d = a.replaceAll("([0-9])", "*");
        String e = b + d + c;
        return e.trim();
    }

    public static String rightPadding(String word, int length, String ch) {
        return (length > word.length()) ? rightPadding(word + ch, length, ch) : word;
    }

    public static String parametersToJson(String parameters) {
        String json = "{\"";
        try {
            if (!"".equals(parameters)) {
                parameters = parameters.replace("\r", "");
                parameters = parameters.replace("\n", "");
                parameters = parameters.replace("+", " ");
                parameters = parameters.replace("+=", "=");
                parameters = parameters.replace("&", "\",\"");
                parameters = parameters.replace("=", "\":\"");
                json += parameters;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        json += "\"}";
        return json;
    }


    public static boolean isEmail(String email) {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return email.matches(emailPattern);
    }

    /**
     * Check with format dd/MM/yyyy
     */
    public static boolean isDate(String date) {
        String datePattern = "^\\d{1,2}\\/\\d{1,2}\\/\\d{4}$";
        return date.matches(datePattern);
    }

    public static boolean isPhoneNumber(String phoneNum) {
        String datePattern = "^\\d{10,11}$";
        return phoneNum.matches(datePattern);
    }

    public static boolean isNumber(String number) {
        if (number == null) return false;
        String numberPattern = "\\d+";
        return number.matches(numberPattern);
    }

    public static Date getYesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }


    private static Collection<String> convertCollec(String strChannel) {
        Collection<String> coll = null;

        try {
            String[] arrColl = strChannel.split("%2C");
            coll = Arrays.asList(arrColl);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return coll;

    }


    public static int isValidDate(String d) //DD/MM/YYYY
    {
        if (d == null || "".equals(d)) return -1;
        String regex = "^(3[01]|[12][0-9]|0[1-9]|[1-9])/(1[0-2]|0[1-9]|[1-9])/[0-9]{4}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(d);
        boolean result = matcher.matches();
        if (result) return 1;
        return 0;
    }

    public static String convertBigDecimaltoString(BigDecimal number, String pattern) {
        if (number == null) return "";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(Double.parseDouble(number.toString()));
    }

    /**
     * @param key : key need to format
     * @return keyHaveFormat or Null
     */
    public static String formatString(String key) {
        if (key != null && !key.trim().isEmpty()) {
            key = "%" + key.toLowerCase() + "%";
            return key;
        }
        return null;
    }
}
