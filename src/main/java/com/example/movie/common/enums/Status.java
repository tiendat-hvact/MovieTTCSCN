package com.example.movie.common.enums;

public class Status {
    // Hoạt động (vd: phim đang hoạt động)
    public static final String ACTIVE = "ACTIVE";
    // Xóa (vd: phim bị xóa)
    public static final String DELETE = "DELETE";
    // Report (vd: phim bị report)
    public static final String REPORT = "REPORT";
    // Chờ (vd: report chờ xử lý)
    public static final String WAIT = "WAIT";
    // Đã xử lý (vd: report đã được xử lý)
    public static final String DONE = "DONE";
}
